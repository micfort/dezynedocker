# syntax=docker/dockerfile:1
FROM ubuntu:22.04
COPY . /dezyne

ENV TZ=Etc/UTC

ARG DEZYNE_BRANCH=tags/v2.15.4
ARG PARALLEL_BUILD=8

RUN echo 'Installing dependencies' \
  && apt-get update \
  && apt-get install -y software-properties-common \
  && add-apt-repository ppa:mcrl2/release-ppa \
  && apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get -y install \
      guile-3.0-dev \
      guile-json \
      mcrl2 \
      pkg-config \
      make \
      mono-mcs \
      nodejs \
      g++ \
      wget \
      git \
      autoconf \
      automake \
      gettext \
      texinfo \
      help2man \
  && apt-get remove -y software-properties-common \
  && apt autoremove -y \
  && rm -rf /var/lib/apt/lists/* \
  && useradd -u 1000 default-user 

RUN echo 'Starting build' \
  && git clone https://git.savannah.gnu.org/git/dezyne.git src \
  && cd src \
  && git checkout $DEZYNE_BRANCH \
  && ./autogen.sh \
  && ./configure --enable-languages=cs,schema,javascript,c++,c --with-courage \
  && make -j$PARALLEL_BUILD && make install \
  && cd / \
  && rm -rf /src 

VOLUME /work
WORKDIR /work

USER 1000:1000
